
from flask import Flask, request,  send_from_directory
import os
from config import ES_INDEX, ES_PASSWORD, ES_USERNAME
from flask import jsonify

app = Flask(__name__, static_url_path='', static_folder='search-engine/build')

import ssl
from elasticsearch import Elasticsearch
from elasticsearch.connection import create_ssl_context
context = create_ssl_context()
context.check_hostname = False
context.verify_mode = ssl.CERT_NONE
es = Elasticsearch(
    ['tux-es1.cci.drexel.edu','tux-es2.cci.drexel.edu','tux-es3.cci.drexel.edu'],
    http_auth=(ES_USERNAME, ES_PASSWORD),
    scheme="https",
    port=9200,
    ssl_context = context,
)


@app.route("/", defaults={'path':''})
def serve(path):
    return send_from_directory(app.static_folder,'index.html')

@app.route('/api/search', methods = ['GET'])
def search():


    keywords = request.args.get('q', default = "", type = str)
    start_idx = request.args.get('startIdx', default = 0, type = int)
    end_idx = request.args.get('endIdx', default = 25, type = int)
    query = {
        "from" : start_idx, "size" : end_idx,
        "query": {
            "multi_match" : {
            "query": keywords,
            "fields": ["author", "title", "abstract"]
            }
        }
    }

    res = es.search(index=ES_INDEX,body=query)

    return jsonify(res["hits"]["hits"])

    
# Or specify port manually:
if __name__ == '__main__':
    port = int(os.environ.get('PORT', 5099))
    app.run(host='localhost', port=5099)
