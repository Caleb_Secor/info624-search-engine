# search-engine-ui



## Getting started

Insure node is installed and you can use 'npm' - https://nodejs.org/en/download <br>
Make sure python is installed<br>
pip install virtualenv<br>
virtualenv venv<br>
source venv/bin/activate<br>
pip install -r requirements.txt<br>
fill in values in config.py<br>
cd ./search-engine <br>
npm i<br>
npm run build<br>
cd ..<br>
python server.py<br>
