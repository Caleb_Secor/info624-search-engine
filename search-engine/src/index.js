import React from "react";
import ReactDOM from "react-dom/client";
//import "./index.css";

import { createBrowserRouter, RouterProvider } from "react-router-dom";
import { Layout } from "./Layout";
import SearchEngine from "./component/Search";



let router = createBrowserRouter([
    {
        element: <Layout />,
        children: [
            {
                path: "/",
                element: <SearchEngine />,
            },
            {
                path: "*",
                element: <p>Page not found</p>,
            },
        ],
    },
]);

let root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
    <React.StrictMode>
        <RouterProvider router={router} />
    </React.StrictMode>
);