

import { Outlet } from "react-router-dom";
import Container from 'react-bootstrap/Container';
import Navbar from 'react-bootstrap/Navbar';
import 'bootstrap/dist/css/bootstrap.min.css';


let Header = function () {


    return (
        <>
            <Navbar bg="light" expand="lg">
            <Container>
            <Navbar.Brand href="/">Search Engine</Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
            </Navbar.Collapse>
            </Container>
            </Navbar>
        </>
    );
};

export let Layout = function () {
    return (
        <>
            <Header />
            <main>
                <Outlet />
            </main>
        </>
    );
};